import tensorflow as tf
from tensorflow.keras.preprocessing import image

import numpy as np

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('model', help='model to use for prediction')
parser.add_argument('img', help='image to predict')

args = parser.parse_args()

model = tf.keras.models.load_model(args.model)

i = image.load_img(args.img, target_size=(250, 250))
x = image.img_to_array(i)
x = np.expand_dims(x, axis=0)

pred = model.predict(x)
class_ = model.predict_classes(x)

print(pred)
print(class_)
