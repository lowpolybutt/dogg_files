import keras
import tensorflow as tf
from keras import backend as K
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import pandas as pd
import os

labels_and_data = pd.read_csv('labels.csv')
# data = labels_and_data.iloc[:, 0].values
# labels = labels_and_data.iloc[:, 1].values
    
# def generate_arrays_from_file():
#     for i, r in labels_and_data.iterrows():
#         modded_id = 'train/train/'+r['id']+'.jpg'
        
# def create_train_folder_for_datagen():
#     for i, r in labels_and_data.iterrows():
#         l = r['breed']
#         if not os.path.exists('train/'+l):
#             os.makedirs('train/'+l)
            
#def populate_train_folders():
 #   for i, r in labels_and_data.iterrows():
  #      modded_id='train/train/'+r['id']+'.jpg'
   #     breed = r['breed']
    #    print(modded_id+': '+'train/'+breed+'/'+r['id']+'.jpg')
     #   shutil.copyfile(modded_id, 'train/'+breed+'/'+r['id']+'.jpg')
        
train_d_generator = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        vertical_flip=True)

train_generator = train_d_generator.flow_from_directory(
        'train',
        target_size=(250, 250),
        batch_size=64,
        class_mode='categorical',
        shuffle=True,
        color_mode='rgb')

valid_d_generator = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        vertical_flip=True
)

valid_generator = valid_d_generator.flow_from_directory(
        'validation',
        target_size=(250, 250),
        color_mode='rgb',
        batch_size=1,
        class_mode='categorical',
        shuffle=True
)

test_d_generator = ImageDataGenerator(
        rescale=1./255)

test_generator = test_d_generator.flow_from_directory(
        'test',
        target_size=(250, 250),
        color_mode='rgb',
        batch_size=6,
        class_mode=None,
        shuffle=False
)

STEP_SIZE_TRAIN = train_generator.n // train_generator.batch_size
STEP_SIZE_VALID = valid_generator.n // valid_generator.batch_size

#tb = tf.keras.callbacks.TensorBoard(log_dir='./logs_new', batch_size=32)
# lrop = tf.keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.2, patience=5, min_lr=0.001)

model = tf.keras.Sequential()
model.add(tf.keras.layers.Conv2D(32, (3, 3), input_shape=(3, 250, 250)))
model.add(tf.keras.layers.Activation('relu'))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))

model.add(tf.keras.layers.Conv2D(32, (3, 3)))
model.add(tf.tf.keras.layers.Activation('relu'))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))

model.add(tf.keras.layers.Conv2D(64, (3, 3)))
model.add(tf.keras.layers.Activation('relu'))
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))

#model.add(tf.keras.layers.Conv2D(128, (3, 3)))
#model.add(tf.keras.layers.Activation('relu'))
#model.add(tf.keras.layers.MaxPooling2D(pool_size=(2,2)))

#model.add(tf.keras.layers.Conv2D(256, (3, 3)))
#model.add(tf.keras.layers.Activation('relu'))
#model.add(tf.keras.layers.MaxPooling2D(pool_size=(2,2)))

#model.add(tf.keras.layers.Conv2D(526, (3, 3)))
#model.add(tf.keras.layers.Activation('relu'))
#model.add(tf.keras.layers.MaxPooling2D(pool_size=(2,2)))

model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(32))
model.add(tf.keras.layers.Activation('relu'))
model.add(tf.keras.layers.Dropout(0.45))
model.add(tf.keras.layers.Dense(120))
model.add(tf.keras.layers.Activation('softmax'))
# model.fit_generator(train_generator, steps_per_epoch=320, epochs=125, callbacks=[tb, lrop])

def save_tflite():
        print('todo')

def load():
        model.load_weights('initial_weights.h5')
        model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

def new_weights():
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
        model.fit_generator(train_generator, epochs=10, steps_per_epoch=STEP_SIZE_TRAIN, validation_data=valid_generator, validation_steps=STEP_SIZE_VALID, use_multiprocessing=True)
        model.save_weights('initial_weights.h5')
        model.save('model_.h5')
        save_tflite()

def get_prediction():
        test_generator.reset()
        prediction = model.predict_generator(test_generator, verbose=1, steps=test_generator.n//test_generator.batch_size)
        predicted_class_indices = np.argmax(prediction, axis=1)

        labels = (train_generator.class_indices)
        labels = dict((v, k) for k, v in labels.items())
        predictions = [labels[k] for k in predicted_class_indices]

        filenames = test_generator.filenames
        results = pd.DataFrame({"Filename":filenames,
                                "Predictions": predictions})

        results.to_csv("res.csv", index=False)
        # for now
        save_tflite()

def evaluate():
        print(model.evaluate_generator(generator=valid_generator, steps=STEP_SIZE_VALID))

        
if os.path.exists('initial_weights.h5'):
        load()
        print('exists')
else:
        print('not')
        new_weights()

evaluate()
get_prediction()
