import tensorflow as tf
import numpy as np

from keras.applications.mobilenet import preprocess_input
from keras.preprocessing import image

tf.enable_eager_execution()

interpreter = tf.lite.Interpreter(model_path='nasnet.tflite')
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

input_shape = input_details[0]['shape']

i = image.load_img('p.jpg', target_size=(224, 224))
x = image.img_to_array(i)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)

interpreter.set_tensor(input_details[0]['index'], x)

interpreter.invoke()
output_data = interpreter.get_tensor(output_details[0]['index'])
print(np.argmax(output_data))
print(output_data[0][np.argmax(output_data)])
